using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float mainThrust = 100;
    [SerializeField] float rotationSpeed = 100;
    [SerializeField] AudioClip mainEngine;
    [SerializeField] ParticleSystem mainThrustParticles;

    Rigidbody body;
    AudioSource thrustVoice;


    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody>();
        thrustVoice = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        Thrust();
        Rotate();
    }

    void Thrust()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (!thrustVoice.isPlaying)
            {
                thrustVoice.PlayOneShot(mainEngine);
                mainThrustParticles.Play();
            }
            body.AddRelativeForce(Vector3.up * mainThrust * Time.deltaTime);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            thrustVoice.Stop();
            mainThrustParticles.Stop();
        }
    }

    void Rotate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            ApplyRotation(rotationSpeed);
       
        }
        else if (Input.GetKey(KeyCode.D))
        {
            ApplyRotation(-rotationSpeed);
       
        }
    }

    void ApplyRotation(float rotation)
    {
        body.freezeRotation = true;
        transform.Rotate(Vector3.forward * Time.deltaTime * rotation);
        body.freezeRotation = false;
    }
}
