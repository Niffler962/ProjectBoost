using UnityEngine.SceneManagement;
using UnityEngine;

public class CollisionHandler : MonoBehaviour
{
    [SerializeField] float Delay = 1f;
    [SerializeField] AudioClip explosion;
    [SerializeField] AudioClip success;
    [SerializeField] ParticleSystem successParticles;
    [SerializeField] ParticleSystem explosionParticles;

    AudioSource source;

    bool isTransitioning = false;

    private void Start()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (isTransitioning) return;

        switch (collision.gameObject.tag)
        {
            case "Finish":
                Debug.Log("Collided with LandingPad and finished the level!");
                SuccessSequence();
                break;
            case "Fuel":
                Debug.Log("Picked up fuel");
                break;
            case "Friendly":
                break;
            default:
                CrashSequence();
                break;
        }
    }

    void CrashSequence()
    {
        isTransitioning = true;
        explosionParticles.Play();
        GetComponent<Movement>().enabled = false;
        source.PlayOneShot(explosion);
        Invoke("ReloadLevel", 1f);
    }
    
    void SuccessSequence()
    {
        isTransitioning = true;
        successParticles.Play();
        GetComponent<Movement>().enabled = false;
        source.PlayOneShot(success);
        Invoke("loadNextLevel", 1f);
    }

    void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void loadNextLevel()
    {
        int currentIndex = SceneManager.GetActiveScene().buildIndex;
        int nextIndex = ++currentIndex;
        if(nextIndex == SceneManager.sceneCountInBuildSettings)
        {
            nextIndex = 0;
        }
        SceneManager.LoadScene(nextIndex);
    }
}
